#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Part of Latex-Suite
#
# Copyright: Srinath Avadhanula, Gerd Wachsmuth, rofu
# Description:
#   This file implements a simple outline creation for latex documents.

import re
import os
import sys
import StringIO

# getAuxContents {{{
def getAuxContents(fname):
    if type(fname) is not str:
        fname = fname.group(1)

    # Strategy for determining the name of the aux file:
    # If the suffix is '.tex' then throw it away.
    # If the suffix is not '.aux' then add '.aux'.
    fname = re.sub(r'\.tex$','',fname)
    if not re.search(r'\.aux$', fname):
        fname += '.aux'
    if not os.path.isfile(fname):
        return ''

    # Now we are in position to scan the file.
    try:
        # This longish thing is to make sure that all files are converted into
        # \n seperated lines.
        contents = '\n'.join(open(fname).read().splitlines())
    except IOError:
        return ''

    # TODO what are all the ways in which an aux file can include another?
    pat = re.compile(r'^\\@input{(.*?)}', re.M)
    # pat = re.compile(r'^\\(@?)(include|input){(.*?)}', re.M)
    contents = re.sub(pat, getAuxContents, contents)

    return ('%%==== FILENAME: %s' % fname) + '\n' + contents

# }}}
# getTexContents {{{
def getTexContents(fname):
    if type(fname) is not str:
        fname = fname.group(3)

    # If neither the file or file.tex exists, then we just give up.
    if not os.path.isfile(fname):
        if os.path.isfile(fname + '.tex'):
            fname += '.tex'
        else:
            return ''

    try:
        # This longish thing is to make sure that all files are converted into
        # \n seperated lines.
        contents = '\n'.join(open(fname).read().splitlines())
    except IOError:
        return ''

    # TODO what are all the ways in which a tex file can include another?
    pat = re.compile(r'^\\(@?)(include|input|subfile){(.*?)}', re.M)
    contents = re.sub(pat, getTexContents, contents)

    return ('%%==== FILENAME: %s' % fname) + '\n' + contents

# }}}
# utfify {{{
def utfify(text):
    for (pat,rep) in [['"a','ä'],['"o','ö'],['"u','ü'],['"A','Ä'],['"O','Ö'],['"U','Ü'], ['\'e', 'é']]:
        if isinstance(text,str):
            text = re.sub(r'\\IeC {\\' + pat + '}', rep, text)
        elif isinstance(text,(list,tuple)):
            text = [re.sub(r'\\IeC {\\' + pat + '}', rep, line) for line in text]
    return text
# }}}
# stripComments {{{
#def stripComments(contents):
#    # remove all comments
#    # comment is a '%' preceeded by an even number of '\'
#    uncomm = [re.sub(r'(?<!\\)(\\\\)*%.*', '', line) for line in contents.splitlines()]
#
#    nonempty = [line for line in uncomm if line.strip()]
#
#    return nonempty

def stripComments(contents):
    # remove all comments except those of the form
    # %%==== FILENAME: <filename.tex>
    # BUG: This comment right after a new paragraph is not recognized:
    # foo\\%comment
    uncomm = [re.sub('(?<!\\\\)%(?!==== FILENAME: ).*', '', line)
              for line in contents.splitlines()]
    # also remove all only-whitespace lines.
    nonempty = [line for line in uncomm if line.strip()]

    return nonempty
# }}}
# addFileNameAndNumber {{{
def addFileNameAndNumber(lines):
    filename = ''
    retval = ''
    for line in lines:
        if re.match('%==== FILENAME: ', line):
            filename = line.split('%==== FILENAME: ')[1]
        else:
            retval += '<%s>%s\n' % (filename, line)

    return retval
#}}}

# getSectionLabels_Root {{{
def getSectionLabels_Root(lineinfo, section_prefix, label_prefix, value_prefix, mapping):
    prev_txt = ''
    inside_env = 0
    prev_env = ''
    outstr = StringIO.StringIO('')
    pres_depth = len(section_prefix)

    #print '+getSectionLabels_Root: lineinfo = [%s]' % lineinfo
    for line in lineinfo.splitlines():
        if not line:
            continue

        # throw away leading white-space
        m = re.search('<(.*?)>(.*)', line)

        fname = m.group(1)
        line = m.group(2).lstrip()

        # we found a label!
        m = re.search(r'\\label{(%s.*?)}' % label_prefix, line)
        if m:
            # add the current line (except the \label command) to the text
            # which will be displayed below this label
            prev_txt += re.search(r'(^.*?)\\label{', line).group(1)

            # for the figure environment however, just display the caption.
            # instead of everything since the \begin command.
            if prev_env == 'figure':
                cm = re.search(r'\caption(\[.*?\]\s*)?{(.*?)}', prev_txt)
                if cm:
                    prev_txt = cm.group(2)

            # print a nice formatted text entry like so
            #
            # >        eqn:label
            # :          e^{i\pi} + 1 = 0
            #
            # Use the current "section depth" for the leading indentation.
            if m.group(1) in mapping:
                aux_info = mapping[m.group(1)]
            else :
                aux_info = ''
            if re.match(value_prefix, aux_info):
                print >>outstr, '>%s%s\t\t<%s>' % (' '*(2*pres_depth+2),
                        m.group(1), fname)
                print >>outstr, ':%s%s %s' % (' '*(2*pres_depth+4), aux_info, prev_txt)
            prev_txt = ''

        # If we just encoutered the start or end of an environment or a
        # label, then do not remember this line.
        # NOTE: This assumes that there is no equation text on the same
        # line as the \begin or \end command. The text on the same line as
        # the \label was already handled.
        if re.search(r'\\begin{(equation|eqnarray|align|figure)', line):
            prev_txt = ''
            prev_env = re.search(r'\\begin{(.*?)}', line).group(1)
            inside_env = 1

        elif re.search(r'\\label', line):
            prev_txt = ''

        elif re.search(r'\\end{(equation|eqnarray|align|figure)', line):
            inside_env = 0
            prev_env = ''

        else:
            # If we are inside an environment, then the text displayed with
            # the label is the complete text within the environment,
            # otherwise its just the previous line.
            if inside_env:
                prev_txt += line
            else:
                prev_txt = line

    return outstr.getvalue()

# }}}
# getSectionLabels {{{
def getSectionLabels(lineinfo,
        sectypes=['chapter', 'section', 'subsection', 'subsubsection'],
        section_prefix='', label_prefix='', value_prefix='', mapping={}):

    if not sectypes:
        return getSectionLabels_Root(lineinfo, section_prefix, label_prefix, value_prefix, mapping)

    ##print 'sectypes[0] = %s, section_prefix = [%s], lineinfo = [%s]' % (
    ##        sectypes[0], section_prefix, lineinfo)

    sections = re.split(r'(<.*?>\\%s{.*})' % sectypes[0], lineinfo)

    # there will 1+2n sections, the first containing the "preamble" and the
    # others containing the child sections as paris of [section_name,
    # section_text]

    rettext = getSectionLabels(sections[0], sectypes[1:], section_prefix, label_prefix, value_prefix, mapping)

    for i in range(1,len(sections),2):
        sec_num = (i+1)/2
        section_name = re.search(r'\\%s{(.*?)}' % sectypes[0], sections[i]).group(1)
        section_label_text = getSectionLabels(sections[i] + sections[i+1], sectypes[1:],
                                    section_prefix+('%d.' % sec_num), label_prefix, value_prefix, mapping)

        if section_label_text:
            sec_heading = 2*' '*len(section_prefix) + section_prefix
            sec_heading += '%d. %s' % (sec_num, section_name)
            sec_heading += '<<<%d\n' % (len(section_prefix)/2+1)

            rettext += sec_heading + section_label_text

    return rettext

# }}}

# getAuxSectionLabels_Root {{{
def getAuxSectionLabels_Root(lineinfo, section_prefix, label_prefix, value_prefix, mapping):
    outstr = StringIO.StringIO('')
    pres_depth = section_prefix

    for line in lineinfo.splitlines():
        prev_txt = ''
        if not line:
            continue

        # throw away leading white-space
        m = re.search('<(.*?)>(.*)', line)
        try :
            fname = m.group(1)
            line = m.group(2).lstrip()
        except :
            print line

        # we found a label!
        m = re.search(r'\\newlabel{(%s.*?)}' % label_prefix, line)
        if m and not re.search(r'^tocindent-?[0-9]*$', m.group(1)):
            # add the text (without aliascounter:) in the pre-last {} to the text
            # which will be displayed below this label
            n = re.search(r'\\newlabel{(%s.*?)}{{(\\relax )?(.*?)}.*{(aliascounter:)?(.*)}{.*?}}' % label_prefix, line)
            if n:
              # Version with hyperref
              o = re.search(r'equation\.(.*)', n.group(5))
              if o:
                # Found an equation
                prev_txt += '(' + n.group(3) + ')'
              else:
                o = re.search(r'AMS\.(.*)', n.group(5))
                if o:
                  # Found an named equation => try to find the name
                  # After the name, there is the page number (assumed to be of the form [0-9a-zA-Z]* )
                  p = re.search(r'\\newlabel{(%s.*?)}{{{(.*?)}}{[0-9a-zA-Z]*}' % label_prefix, line)
                  if p:
                    equation_number = p.group(2)
                    prev_txt += '(' + equation_number + ')'
                  else:
                    prev_txt += n.group(5)
                else:
                  prev_txt += re.match(r'^\w*', n.group(5)).group() + '.' + n.group(3)
            else:
              # Version without hyperref
              n = re.search(r'\\newlabel{(%s.*?)}{{(\\relax )?(.*)}{.*}}' % label_prefix, line)
              prev_txt += n.group(3)

            if re.match( value_prefix, prev_txt):
                # Print label and counter+number:
                print >>outstr, '>%s%s\t\t<%s>' % (' '*(2*pres_depth-2), m.group(1), fname)
                print >>outstr, ':%s%s' % (' '*(2*pres_depth+0), prev_txt)
                mapping[m.group(1)] = prev_txt

    return outstr.getvalue()
# }}}

# getAuxSectionLabels {{{
def getAuxSectionLabels(lineinfo,
        sectypes=['part', 'chapter', 'section', 'subsection', 'subsubsection', 'paragraph', 'subparagraph'],
        section_prefix=1, label_prefix='', value_prefix='', mapping={}):

    if not sectypes:
        return getAuxSectionLabels_Root(lineinfo, section_prefix, label_prefix, value_prefix, mapping)

    sections = re.split(r'(<.*?>\\@writefile{toc}{\\contentsline {%s}.*)' % sectypes[0], lineinfo)

    # there will 1+2n sections, the first containing the "preamble" and the
    # others containing the child sections as paris of [section_name,
    # section_text]

    rettext = getAuxSectionLabels(sections[0], sectypes[1:], section_prefix, label_prefix, value_prefix, mapping)

    for i in range(1,len(sections),2):

        section_label_text = getAuxSectionLabels(sections[i+1], sectypes[1:], section_prefix+1, label_prefix, value_prefix, mapping)

        if section_label_text:
          # This section contains labels
          # Let us determine the section number and the section heading
          o1 = re.search( r'{%s}{\\numberline {(\\relax )?(.*?)}(.*?)}{[^{}]*}{[^{}]*}}$' % sectypes[0] , sections[i]) # With hyperref
          o2 = re.search( r'{%s}{\\numberline {(\\relax )?(.*?)}(.*?)}' % sectypes[0] , sections[i]) # Without hyperref
          o3 = re.search( r'{%s}{\\toc(section|chapter) {(.*?)}{(.*?)}{(.*?)}' % sectypes[0] , sections[i]) # amsart,amsbook
          o4 = re.search( r'{%s}{(.*?)}' % sectypes[0] , sections[i])
          if o1:
            section_name = o1.group(3)
            section_number = o1.group(2) + ' '
          elif o2:
            section_name = o2.group(3)
            section_number = o2.group(2) + ' '
          elif o3:
            section_name = o3.group(4)
            if o3.group(2) == "":
              section_number =  o3.group(3) + ' '
            else:
              section_number =  o3.group(2) + ' ' + o3.group(3) + ' '
          elif o4:
            section_name = o4.group(1)
            section_number = ''
          else:
            print 'Unknown heading format "%s"' % sections[i]
            section_name = "Unknown Name"
            section_number = "??"
          sec_heading = 2*' '*(section_prefix-1)
          sec_heading += '%s. %s' % (section_number.strip(), section_name)
          sec_heading += '<<<%d\n' % section_prefix

          rettext += sec_heading + section_label_text

    return rettext

# }}}
# main {{{
def main(fname, prefix):
    [head, tail] = os.path.split(fname)
    if head:
        os.chdir(head)

    # Does prefix look like a label or a value?
    o = re.match( '(\([0-9a-zA-Z.]*|\w*\.[0-9a-zA-Z.]*)' , prefix )
    if o:
        label_prefix = ''
        value_prefix = re.escape(prefix)
    else:
        label_prefix = prefix
        value_prefix = ''

    tex_contents = getTexContents(fname)
    tex_contents = stripComments(tex_contents)
    lineinfo = addFileNameAndNumber(tex_contents)
    lineinfo = utfify(lineinfo)

    mapping = {}
    aux_contents = getAuxContents(fname)
    if aux_contents:
        aux_contents = stripComments(aux_contents)
        aux_lineinfo = addFileNameAndNumber(aux_contents)
        aux_lineinfo = utfify(aux_lineinfo)
        getAuxSectionLabels(aux_lineinfo, label_prefix=label_prefix, value_prefix=value_prefix, mapping=mapping)
    rettext = getSectionLabels(lineinfo, label_prefix=label_prefix, value_prefix=value_prefix, mapping=mapping)
    a = re.findall( r'(^|\n)> *([^ ]*)\n' , rettext)

    if len(a) == 0:
        return ''
    elif len(a) == 1:
        # Only one partial match
        # Check, if prefix matches exactly
        if value_prefix != '' and re.search( r'\n: *%s\n' % value_prefix, rettext):
          # Value_prefix matches _exactly_ counter.number => return only this matching label
          return a[0][1].split()[0]
        elif label_prefix != '':
          # Prefix matches the beginning of the label => return only this matching label
          return a[0][1].split()[0]
        else:
          return rettext
    else:
        return rettext

    return
# }}}

if __name__ == "__main__":
    if len(sys.argv) > 2:
        prefix = sys.argv[2]
    else:
        prefix = ''

    print main(sys.argv[1], prefix)


# vim: fdm=marker
